# Deployment Mushrooms for Cognitive Systems

## Setup

Make sure you have installed last version of [python](https://wiki.python.org/moin/BeginnersGuide/Download).

### Option 1: Pipenv

1. Have installed [pipenv](https://pipenv.readthedocs.io/en/latest/#install-pipenv-today) for dependencies managing.
2. Go to the project's root
3. `pipenv install`

### Option 2: Install manually the dependencies

Install the following dependencies: 
1. apyori
2. sklearn
3. python-graphviz
4. graphviz
5. decision-tree-id3
6. numpy

## How to run

1. If using pipenv: `pipenv shell`
2. `cd src/models`
3. `python predict_model.py` 

## Evaluation

To perform the cross-validation test 5-folds:

1. If using pipenv: `pipenv shell`
2. `cd src/features`
3. `python id3_mushrooms.py`

## Detailed Project's Structure

```
.
├── LICENSE
├── Pipfile
├── Pipfile.lock
├── README.md
├── data
│   ├── external
│   ├── interim
│   ├── processed
│   └── raw
│       ├── mushrooms.csv
│       └── mushrooms_v2.csv
├── models
│   ├── pruned_tree.dot
│   ├── tree.dot
│   └── tree_v2.dot
├── notebooks
├── references
├── reports
│   ├── report.pdf
│   ├── figures
│   └── trees
│       ├── pruned_tree.pdf
│       ├── tree.pdf
│       └── tree_v2.pdf
└── src
    ├── data
    ├── features
    │   └── id3_mushrooms.py <--- The main library used by the predict model and the cross validation test
    ├── models
    │   ├── predict_model.py <--- This is the actual application
    │   └── tree_v2_prune.dot
    ├── testing <--- This directory contains several files used to test and explore different possibilities
    │   ├── impId3.py
    │   ├── impld3b.py
    │   ├── iris
    │   ├── iris.pdf
    │   ├── iris.py
    │   ├── start.py
    │   ├── stuff
    │   ├── stuff.pdf
    │   ├── tree.dot
    │   ├── tree.pdf
    │   └── pca_svm.py <--- This was the first approach to PCA+SVM
    └── visualization
        └── exploration.R <--- This file has been used to explore our data
```
