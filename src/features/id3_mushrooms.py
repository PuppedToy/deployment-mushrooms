from id3 import Id3Estimator
from id3 import export_graphviz
import graphviz 
import numpy as np
import csv
import warnings

data = []
results = []

def warn(*args, **kwargs):
    pass
warnings.warn = warn

first = True
with open('../../data/raw/mushrooms_v2.csv') as csvfile:
	readCSV = csv.reader(csvfile, delimiter=',')
	for row in readCSV:
		if first:
			names = row
			names.pop(0)
			first = False
		else:
			poisonous = row.pop(0)
			data.append(row)
			results.append(poisonous)

def fit():
	fitestimator = Id3Estimator()
	fitestimator.fit(data, results)
	export_graphviz(fitestimator.tree_, 'tree_v2_prune.dot', feature_names = names)
	return fitestimator

def shuffle():
	global data
	global results
	for i in range(len(data)):
		j = np.random.randint(len(data))
		aux_data = data[i]
		aux_results = results[i]
		data[i] = data[j]
		results[i] = results[j]
		data[j] = aux_data
		results[j] = aux_results

def cross_validation(partitions, do_shuffle = True, debug = False):
	global data
	global results
	if do_shuffle:
		shuffle()
	subsets = []
	i = 0
	err = 0
	while len(data) > 0:
		try:
			subsets[i]
		except IndexError:
			subsets.append({'data': [], 'results': []})
		subsets[i]["data"].append(data.pop())
		subsets[i]["results"].append(results.pop())
		i = (i + 1) % partitions

	present_iteration = 0

	result = {
		'TP': 0,
		'FP': 0,
		'TN': 0,
		'FN': 0,
		'CP': 0,
		'PCP': 0,
		'CN': 0,
		'PCN': 0,
		'TOTAL': 0,
		'ERROR': 0
	}

	for test_subset in subsets:
		print('Cross validation progress: ' + str(present_iteration/partitions*100) + '%')
		present_iteration += 1
		# Prediction for this value

		# Not test data will be train data
		train_subset = {"data": [], "results": []}
		for subset in subsets:
			if subset != test_subset:
				train_subset["data"] = train_subset["data"] + subset["data"]
				train_subset["results"] = train_subset["results"] + subset["results"]

		estimator = Id3Estimator(gain_ratio = True, is_repeating = True)
		estimator.fit(train_subset["data"], train_subset["results"])

		try:
			prediction = estimator.predict(test_subset["data"])
		except ValueError:
			return cross_validation(partitions, debug)

		if len(prediction) != len(test_subset["results"]):
			print("ERROR!")
			return 0

		suberr = 0
		for i in range(len(prediction)):
			# if debug and test_subset["data"][i][4] == 'n' and test_subset["data"][i][19] == 'w' and test_subset["data"][i][16] != 'w':
			# if debug and test_subset["data"][i][4] == 'n' and test_subset["data"][i][19] == 'u':
			if debug and test_subset["data"][i][19] == 'u':
				print("Evaluating this:")
				global names
				print(names)
				print(test_subset["data"][i])
				print("Prediction: " + test_subset["results"][i])
				input("Press Enter to continue...")
				print("Result: " + prediction[i])
			false_true = 'T'
			positive_negative = 'P'
			if prediction[i] != test_subset["results"][i]:
				false_true = 'F'
			if prediction[i] == 'e':
				positive_negative = 'N'
			result_type = false_true + positive_negative
			result[result_type] += 1
			result['TOTAL'] += 1
	
	result['CP'] = result['TP'] + result['FN']
	result['CN'] = result['FP'] + result['TN']
	result['PCP'] = result['TP'] + result['FP']
	result['PCN'] = result['TN'] + result['FN']

	result['PRE'] = result['CP']/result['TOTAL']*100
	result['ACC'] = (result['TP'] + result['TN'])/result['TOTAL']*100
	result['REC'] = result['TP']/result['CP']*100
	result['FOUT'] = result['TN']/result['CN']*100
	result['MISS'] = result['FN']/result['CP']*100
	result['SPEC'] = result['FP']/result['CN']*100

	result['PPV'] = result['TP']/result['PCP']*100
	result['FDR'] = result['FP']/result['PCP']*100
	result['NPV'] = result['TN']/result['PCN']*100
	result['FOR'] = result['FN']/result['PCN']*100

	result['PLR'] = result['REC']/result['FOUT']*100
	result['NLR'] = result['MISS']/result['SPEC']*100
	result['DOR'] = result['PLR']/result['NLR']*100
	result['F1S'] = 2/(1/result['REC'] + 1/result['PPV'])

	print('Cross validation progress: 100%')
	return result

if __name__ == "__main__":
	N_FOLDS = 5
	print('Cross validation ' + str(N_FOLDS) + '-folds')
	cv = cross_validation(N_FOLDS)
	print('')
	print('Error: ' + str(100 - cv['ACC']) + '%')
	print('True Positives (predicted correctly a poisonous mushroom): ' + str(cv['TP']))
	print('False Positives (predicted poisonous but actually was edible): ' + str(cv['FP']))
	print('True Negative (predicted correctly a edible mushroom): ' + str(cv['TN']))
	print('False Negatives (predicted edible but actually was poisonous): ' + str(cv['FN']))
	print('Condition Positive: ' + str(cv['CP']))
	print('Condition Negative: ' + str(cv['CN']))
	print('Predicted Condition Positive: ' + str(cv['PCP']))
	print('Predicted Condition Negative: ' + str(cv['PCN']))
	print('TOTAL PREDICTIONS: ' + str(cv['TOTAL']))
	print('Prevalence: ' + str(cv['PRE']) + '%')
	print('Accuracy: ' + str(cv['ACC']) + '%')
	print('Recall: ' + str(cv['REC']) + '%')
	print('Fall-out: ' + str(cv['FOUT']) + '%')
	print('Miss rate: ' + str(cv['MISS']) + '%')
	print('Specifity: ' + str(cv['SPEC']) + '%')
	print('Positive predictive value: ' + str(cv['PPV']) + '%')
	print('False discovery rate: ' + str(cv['FDR']) + '%')
	print('Negative predictive value: ' + str(cv['NPV']) + '%')
	print('False omission rate: ' + str(cv['FOR']) + '%')
	print('Positive Likelihood Ratio: ' + str(cv['PLR']) + '%')
	print('Negative Likelihood Ratio: ' + str(cv['NLR']) + '%')
	print('Diagnostic odds ratio: ' + str(cv['DOR']) + '%')
	print('F1 SCORE: ' + str(cv['DOR']))


