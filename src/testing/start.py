

from sklearn import tree
import graphviz 

dicc = {
	'rana': 1,
	'sapo': 2,
	'mosquito': 3,
	'hambre': 1,
	'nohambre': 2,
	'comer': 1,
	'nocomer': 2
}


# X = [[0, 0], [1, 1], [0.3, 0.6], [1.3, 0.2], [2, 3], [4, 1]]
# Y = [0, 1, 0, 0, 1, 0]
X = [[dicc['rana'], dicc['hambre'], dicc['mosquito']], [dicc['rana'], dicc['hambre'], dicc['sapo']], [dicc['rana'], dicc['nohambre'], dicc['mosquito']], [dicc['sapo'], dicc['nohambre'], dicc['mosquito']], [dicc['mosquito'], dicc['hambre'], dicc['rana']]]
Y = [dicc['comer'], dicc['nocomer'], dicc['comer'], dicc['nocomer'], dicc['nocomer']]
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)

print(clf.predict([[dicc['rana'], dicc['hambre'], dicc['mosquito']]]))

dot_data = tree.export_graphviz(clf, out_file=None) 
graph = graphviz.Source(dot_data)
graph.render("stuff") 
