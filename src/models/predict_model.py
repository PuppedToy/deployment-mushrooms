import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'features'))

import id3_mushrooms

estimator = id3_mushrooms.fit()

shorts = {
	'bell': 'b',
	'conical': 'c',
	'convex': 'x',
	'flat': 'f',
	'knobbed': 'k',
	'sunken': 's',

	'fibrous': 'f',
	'grooves': 'g',
	'scaly': 'y',
	'smooth': 's',

	'brown': 'n',
	'buff': 'b',
	'cinnamon': 'c',
	'gray': 'g',
	'green': 'r',
	'pink': 'p',
	'purple': 'u',
	'red': 'e',
	'white': 'w',
	'yellow': 'y',

	'bruises': 't',
	'no': 'f',

	'almond': 'a',
	'anise': 'l',
	'creosote': 'c',
	'fishy': 'y',
	'foul': 'f',
	'musty': 'm',
	'none': 'n',
	'pungent': 'p',
	'spicy': 's',

	'attached': 'a',
	'descending': 'd',
	'free': 'f',
	'notched': 'n',

	'close': 'c',
	'crowded': 'w',
	'distant': 'd',

	'broad': 'b',
	'narrow': 'n',

	'black': 'k',
	'chocolate': 'h',
	'orange': 'o',

	'enlarging': 'e',
	'tapering': 't',

	'bulbous': 'b',
	'club': 'c',
	'cup': 'u',
	'equal': 'e',
	'rhizomorphs': 'z',
	'rooted': 'r',
	'missing': '?',

	'fibrous': 'f',
	'scaly': 'y',
	'silky': 'k',
	'smooth': 's',

	'partial': 'p',
	'universal': 'u',

	'one': 'o',
	'two': 't',

	'cobwebby': 'c',
	'evanescent': 'e',
	'flaring': 'f',
	'large': 'l',
	'pendant': 'p',
	'sheathing': 's',
	'zone': 'z',

	'abundant': 'a',
	'clustered': 'c',
	'numerous': 'n',
	'scattered': 's',
	'several': 'v',
	'solitary': 'y',

	'grasses': 'g',
	'leaves': 'l',
	'meadows': 'm',
	'paths': 'p',
	'urban': 'u',
	'waste': 'w',
	'woods': 'd',
}

questions = {
	'cap-shape': ['bell', 'conical', 'convex', 'flat', 'knobbed', 'sunken'],
	'cap-surface': ['fibrous', 'grooves', 'scaly', 'smooth'],
	'cap-color': ['brown', 'buff', 'cinnamon', 'gray', 'green', 'pink', 'purple', 'red', 'white', 'yellow'],
	'bruises': ['bruises', 'no'],
	'odor': ['almond', 'anise', 'creosote', 'fishy', 'foul', 'musty', 'none', 'pungent', 'spicy'],
	'gill-attachment': ['attached', 'descending', 'free', 'notched'],
	'gill-spacing': ['close', 'crowded', 'distant'],
	'gill-size': ['broad', 'narrow'],
	'gill-color': ['black', 'brown', 'buff', 'chocolate', 'gray', 'green', 'orange', 'pink', 'purple', 'red', 'white', 'yellow'],
	'stalk-shape': ['enlarging', 'tapering'],
	'stalk-root': ['bulbous', 'club', 'cup', 'equal', 'rhizomorphs', 'rooted', 'missing'],
	'stalk-surface-above-ring': ['fibrous', 'scaly', 'silky', 'smooth'],
	'stalk-surface-below-ring': ['fibrous', 'scaly', 'silky', 'smooth'],
	'stalk-color-above-ring': ['brown', 'buff', 'cinnamon', 'gray', 'orange', 'pink', 'red', 'white', 'yellow'],
	'stalk-color-below-ring': ['brown', 'buff', 'cinnamon', 'gray', 'orange', 'pink', 'red', 'white', 'yellow'],
	'veil-type': ['partial', 'universal'],
	'veil-color': ['brown', 'orange', 'white', 'yellow'],
	'ring-number': ['none', 'one', 'two'],
	'ring-type': ['cobwebby', 'evanescent', 'flaring', 'large', 'none', 'pendant', 'sheathing', 'zone'],
	'spore-print-color': ['black', 'brown', 'buff', 'chocolate', 'green', 'orange', 'purple', 'white', 'yellow'],
	'population': ['abundant', 'clustered', 'numerous', 'scattered', 'several', 'solitary'],
	'habitat': ['grasses', 'leaves', 'meadows', 'paths', 'urban', 'waste', 'woods'],
}

results = {
	'e': 'That mushroom will probably be edible.',
	'p': 'BE CAREFUL! That mushroom will probably be poisonous.',
}

def getMushroomFromUser():
	print('Please, describe the mushroom answering the following questions:')
	predict_mushroom = []
	describe_mushroom = []
	for characteristic, options in questions.items():
		short_options = list(map(lambda opt: shorts[opt], options))
		while True:
			print('');
			user_input = input('How is the ' + characteristic.replace('-', ' ') + '? (Options: ' + ', '.join(map(lambda opt: opt + '=' + shorts[opt], options)) + '): ').lower()
			if user_input in options:
				predict_mushroom.append(shorts[user_input])
				describe_mushroom.append(characteristic.replace('-', ' ') + ': ' + user_input)
				break
			elif user_input in short_options:
				predict_mushroom.append(user_input)
				describe_mushroom.append(characteristic.replace('-', ' ') + ': ' + options[short_options.index(user_input)])
				break
			else:
				print('Please, select a valid option.')
	return {'predict_mushroom': predict_mushroom, 'describe_mushroom': describe_mushroom}


if __name__ == "__main__":
	print('Welcome to Mushrooms4All. We predict if a mushroom will be edible or not')
	finished = False
	while not finished:
		mushroom = getMushroomFromUser()
		print('')
		print('')
		print('You have selected a mushroom with these characteristics: ')
		for characteristic in mushroom['describe_mushroom']:
			print('- ' + characteristic)

		print('')
		print('')
		print(results[estimator.predict([mushroom['predict_mushroom']])[0]])
		print('')
		while True:
			print('')
			user_input = input('Another mushroom? (y/n): ').lower()
			if user_input == 'n' or user_input == 'no':
				finished = True
				break
			elif user_input == 'y' or user_input == 'yes':
				break
			else:
				print('Please, select a valid option.')
	print('')
	print('Thank you for using Mushrooms4All.')
	print('')




